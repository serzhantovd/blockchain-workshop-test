package com.serzhantovd.blockchain.workshop.dto;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
public class TrackEvent implements Comparable<TrackEvent> {

    private final String data;
    private final long trackTime;

    public TrackEvent(String data, long trackTime) {
        this.data = data;
        this.trackTime = trackTime;
    }

    public String getData() {
        return data;
    }

    public long getTrackTime() {
        return trackTime;
    }

    @Override
    public String toString() {
        return "TrackEvent{" +
                "data='" + data + '\'' +
                ", trackTime=" + trackTime +
                '}';
    }

    @Override
    public int compareTo(TrackEvent o) {
        return Long.compare(trackTime, o.getTrackTime());
    }
}
