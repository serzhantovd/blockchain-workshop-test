package com.serzhantovd.blockchain.workshop.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.Instant;
import java.util.List;
import java.util.Objects;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@JsonSerialize
public class ChainItem {

    @JsonProperty("previous_block_hash")
    private final String previousBlockHash;
    @JsonProperty("rows")
    private final List<String> rows;
    @JsonProperty("timestamp")
    private final long timestamp;
    @JsonProperty("block_hash")
    private final String blockHash;

    private ChainItem(Builder builder) {
        this.previousBlockHash = builder.previousBlockHash;
        this.rows = builder.data;
        this.timestamp = builder.timestamp;
        this.blockHash = builder.blockHash;
    }

    public String getPreviousBlockHash() {
        return previousBlockHash;
    }

    public List<String> getRows() {
        return rows;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getBlockHash() {
        return blockHash;
    }

    @Override
    public String toString() {
        return "ChainItem{" +
                "previousBlockHash='" + previousBlockHash + '\'' +
                ", rows=" + rows +
                ", timestamp=" + timestamp +
                ", blockHash='" + blockHash + '\'' +
                '}';
    }

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {
        private String previousBlockHash;
        private String blockHash;
        private List<String> data;
        private long timestamp = Instant.now().toEpochMilli();

        public Builder setPreviousBlockHash(String previousHash) {
            this.previousBlockHash = previousHash;
            return this;
        }

        public Builder setBlockHash() throws Exception {
            Objects.requireNonNull(previousBlockHash);
            Objects.requireNonNull(data);
            this.blockHash = hash();
            return this;
        }

        public Builder setData(List<String> data) {
            this.data = data;
            return this;
        }

        public ChainItem build() {
            return new ChainItem(this);
        }

        private String hash() throws NoSuchAlgorithmException {
            String str = previousBlockHash
                    .concat(data.toString())
                    .concat(String.valueOf(timestamp));

            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(str.getBytes());

            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < hash.length; i++) {
                String hex = Integer.toHexString(0xff & hash[i]);
                if (hex.length() == 1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        }
    }

}
