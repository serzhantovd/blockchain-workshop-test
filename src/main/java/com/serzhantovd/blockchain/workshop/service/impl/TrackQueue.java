package com.serzhantovd.blockchain.workshop.service.impl;

import com.serzhantovd.blockchain.workshop.dto.TrackEvent;
import com.serzhantovd.blockchain.workshop.service.ITrackQueue;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@Service
public class TrackQueue implements ITrackQueue {

    private static final Logger LOG = LoggerFactory.getLogger(TrackQueue.class);

    private final PriorityBlockingQueue<TrackEvent> queue = new PriorityBlockingQueue<>();

    @Override
    public void track(TrackEvent event) {
        queue.add(event);
        LOG.info("tracked event = {}", event);
    }

    @Override
    public synchronized List<TrackEvent> get(int count) {
        if (queue.size() >= count) {
            List<TrackEvent> result = new ArrayList<>();
            queue.drainTo(result, count);
            return result;
        }
        return Collections.emptyList();
    }

}
