package com.serzhantovd.blockchain.workshop.service;

import com.serzhantovd.blockchain.workshop.dto.ChainItem;
import com.serzhantovd.blockchain.workshop.dto.TrackEvent;

import java.util.List;
import java.util.function.Function;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
public interface IChainProcessor extends Function<List<TrackEvent>, ChainItem> {

    void process();

    void shutdown();

}
