package com.serzhantovd.blockchain.workshop.service.impl;

import com.serzhantovd.blockchain.workshop.dto.ChainItem;
import com.serzhantovd.blockchain.workshop.service.IChainCache;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@Service
public class MapChainCache implements IChainCache {

    private static final Logger LOG = LoggerFactory.getLogger(MapChainCache.class);

    private final Map<String, ChainItem> chain = new ConcurrentHashMap<>();
    private volatile String lastHash;

    public MapChainCache() {
        this.lastHash = StringUtils.EMPTY;
    }

    @Override
    public List<ChainItem> get(int chainCount) {
        List<ChainItem> result = new ArrayList<>();

        ChainItem item = chain.get(lastHash);
        if (Objects.isNull(item)) {
            return Collections.emptyList();
        }

        result.add(item);
        while (result.size() < chainCount && Objects.nonNull(item)) {
            item = chain.get(item.getPreviousBlockHash());
            if (Objects.nonNull(item)) {
                result.add(item);
            }
        }
        return result;
    }

    @Override
    public void put(ChainItem item) {
        if (Objects.nonNull(item)) {
            chain.put(item.getBlockHash(), item);
            lastHash = item.getBlockHash();
        }
    }
}
