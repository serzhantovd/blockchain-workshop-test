package com.serzhantovd.blockchain.workshop.service;

import com.serzhantovd.blockchain.workshop.dto.TrackEvent;

import java.util.List;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
public interface ITrackQueue {

    void track(TrackEvent event);

    List<TrackEvent> get(int count);
}
