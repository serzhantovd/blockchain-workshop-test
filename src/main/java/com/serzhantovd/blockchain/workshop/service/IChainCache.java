package com.serzhantovd.blockchain.workshop.service;

import com.serzhantovd.blockchain.workshop.dto.ChainItem;

import java.util.List;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
public interface IChainCache {

    List<ChainItem> get(int chainCount);

    void put(ChainItem items);
}
