package com.serzhantovd.blockchain.workshop.service.impl;

import com.serzhantovd.blockchain.workshop.dto.ChainItem;
import com.serzhantovd.blockchain.workshop.dto.TrackEvent;
import com.serzhantovd.blockchain.workshop.service.IChainCache;
import com.serzhantovd.blockchain.workshop.service.IChainProcessor;
import com.serzhantovd.blockchain.workshop.service.ITrackQueue;
import com.typesafe.config.Config;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import javax.annotation.PostConstruct;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@Service
public class AsyncChainProcessor implements IChainProcessor {

    private static final Logger LOG = LoggerFactory.getLogger(MapChainCache.class);

    private final int count;
    private final long periodMs;
    private final IChainCache cache;
    private final ITrackQueue queue;
    private final ScheduledExecutorService service;

    private volatile String previousHash = "0";

    @Autowired
    public AsyncChainProcessor(IChainCache cache, ITrackQueue queue, Config config) {
        this.cache = cache;
        this.queue = queue;
        this.count = config.getInt("processing.count");
        this.periodMs = config.getLong("processing.call-period-ms");
        this.service = Executors.newSingleThreadScheduledExecutor();
    }

    @PostConstruct
    public void run() {
        service.scheduleAtFixedRate(this::process, periodMs, periodMs, TimeUnit.MILLISECONDS);
    }

    @Override
    public ChainItem apply(List<TrackEvent> events) {
        try {
            ChainItem result = ChainItem.builder()
                    .setData(events.stream().map(TrackEvent::getData).collect(Collectors.toList()))
                    .setPreviousBlockHash(previousHash)
                    .setBlockHash()
                    .build();
            previousHash = result.getBlockHash();
            return result;
        } catch (Exception ex) {
            LOG.error("Process error : {}", ex);
            return null;
        }
    }

    @Override
    public void process() {
        List<TrackEvent> trackEvents = queue.get(count);
        if (!trackEvents.isEmpty()) {
            cache.put(apply(trackEvents));
        }
    }

    @Override
    public void shutdown() {
        try {
            service.shutdown();
            service.awaitTermination(1000, TimeUnit.MILLISECONDS);
        } catch (Exception ex) {
            LOG.error("Shutdown error {}", ex);
        }
        LOG.info("Shutdown ok!");
    }

}
