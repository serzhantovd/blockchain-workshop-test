package com.serzhantovd.blockchain.workshop;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import javax.servlet.ServletContext;
import java.io.File;
import java.util.Arrays;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@SpringBootApplication
public class Application {

    private final static Logger LOG = LoggerFactory.getLogger(Application.class);

    public static void main(String[] args) {
        LOG.info("hellog from main");
        SpringApplication.run(Application.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        LOG.info("Let's inspect the beans provided by Spring Boot:");
        return args -> {
            String[] beanNames = ctx.getBeanDefinitionNames();
            Arrays.sort(beanNames);
            for (String beanName : beanNames) {
                LOG.info("{}", beanName);
            }
        };
    }

    @Bean
    public Config applicationConfig() throws Exception {
        final String path = System.getProperty("confpath");
        final Config appConfig = ConfigFactory.parseFile(new File(path));
        final Config systemConfig = ConfigFactory.load(appConfig);
        return systemConfig.resolve();
    }
}
