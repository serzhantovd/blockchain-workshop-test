package com.serzhantovd.blockchain.workshop.controller;

import com.serzhantovd.blockchain.workshop.dto.ChainItem;
import com.serzhantovd.blockchain.workshop.service.IChainCache;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@RestController
public class GetController {

    private static final Logger LOG = LoggerFactory.getLogger(GetController.class);

    private final ThreadLocal<ObjectMapper> mapper = new ThreadLocal<ObjectMapper>() {
        @Override
        protected ObjectMapper initialValue() {
            return new ObjectMapper();
        }
    };

    private final IChainCache chainCache;

    @Autowired
    public GetController(IChainCache chainCache) {
        this.chainCache = chainCache;
    }

    @GetMapping("last_blocks/{quantity}")
    public String  getEvents(@PathVariable(value = "quantity") int quantity) {
        try {
            List<ChainItem> chainItems = chainCache.get(quantity);
            String json = mapper.get().writeValueAsString(chainItems);
            return json;
        } catch (Exception ex) {
            LOG.error("Get las blocks error: {}", ex);
            return "Some error has occurred! Please, contact to yours administrator";
        }
    }
}
