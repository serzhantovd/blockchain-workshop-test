package com.serzhantovd.blockchain.workshop.controller;

import com.serzhantovd.blockchain.workshop.dto.TrackEvent;
import com.serzhantovd.blockchain.workshop.service.ITrackQueue;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.Instant;

/**
 * Created by Serzhantovd on 20.01.2018.
 */
@RestController
public class TrackController {

    private final Logger LOG = LoggerFactory.getLogger(TrackController.class);
    private final ThreadLocal<ObjectMapper> mapper = new ThreadLocal<ObjectMapper>() {
        @Override
        protected ObjectMapper initialValue() {
            return new ObjectMapper();
        }
    };

    private final ITrackQueue trackQueue;

    @Autowired
    public TrackController(ITrackQueue trackQueue) {
        this.trackQueue = trackQueue;
    }

    @PostMapping("/add_data")
    public void track(@RequestBody String json) {
        try {
            TrackEvent event = new TrackEvent(parse(json), Instant.now().toEpochMilli());
            trackQueue.track(event);
        } catch (Exception ex) {
            LOG.error("Track error: {}", ex);
        }
    }

    private String parse(String json) throws IOException {
        LOG.info("posted = {}", json);
        JsonNode jsonNode = mapper.get().readTree(json);
        return jsonNode.get("data").asText();
    }
}
